const express = require('express')
const rounter = express()

// mock data
const products = [
  {
    id: 1001,
    name: 'Node.js for Beginners',
    category: 'Node',
    price: 990
  },
  {
    id: 1002,
    name: 'React 101',
    category: 'React',
    price: 3990
  },
  {
    id: 1003,
    name: 'Getting started with MongoDB',
    category: 'MongoDB',
    price: 1990
  }
]
rounter.get('/', (req, res) => {
  res.json(products)
})

rounter.get('/:id', (req, res) => {
    const { id } = req.params.id
    const result = products.find(product => product.id === id)
    res.json(result)
  })

rounter.post('/', (req, res) => {
  const payload = req.body
  products.push(payload)
  res.json(payload)
})
rounter.put('/', (req, res) => {
 
  const payload  =req.body
  const index =products.findIndex(product => product.id ===payload.id)
  products.splice(index,1, payload)
  res.json(payload)
})
rounter.delete('/:id', (req, res) => {
  const { id } = req.params
  const index =products.findIndex(product => product.id === id)
  products.splice(index,0)
  res.json({ id })
})


module.exports = rounter;